module.exports = [
  {
    startTime: "09.00 AM",
    endTime: "09.15 AM",
    price: "IDR 200.000",
    duration: "15 min",
  },
  {
    startTime: "09.00 AM",
    endTime: "09.30 AM",
    price: "IDR 250.000",
    duration: "30 min",
  },
  {
    startTime: "09.00 AM",
    endTime: "10.00 AM",
    price: "IDR 300.000",
    duration: "1 hour",
  },
];
