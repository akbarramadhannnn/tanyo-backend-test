const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const routes = require("./routes");
const configEnv = require("./config/env");

const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(routes);
app.listen(configEnv.port, () => {
  console.log(`Server running on port ${configEnv.port}`);
});
