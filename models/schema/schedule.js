const mongoose = require("../../connection/mongoose");
const { ObjectId } = mongoose.Schema;

const ShceduleSchema = new mongoose.Schema({
  usersId: {
    type: ObjectId,
    ref: "Users",
  },
  from: {
    type: Date,
  },
  to: {
    type: Date,
  },
  time: {
    type: String,
    enum: ["0", "1", "2"],
  },
  consultType: {
    type: String,
    enum: ["0", "1", "2"],
  },
});

module.exports = mongoose.model("Schedule", ShceduleSchema);
