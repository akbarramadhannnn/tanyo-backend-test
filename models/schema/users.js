const mongoose = require("../../connection/mongoose");

const UsersSchema = new mongoose.Schema({
  fullName: {
    type: String,
  },
  username: {
    type: String,
  },
  password: {
    type: String,
  },
});

module.exports = mongoose.model("Users", UsersSchema);
