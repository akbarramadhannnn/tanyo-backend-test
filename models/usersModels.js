const Users = require("./schema/users");

exports.addUserData = async (payload) => {
  const user = new Users(payload);
  const result = user.save();
  return result;
};

exports.getUserByUsername = async (username) => {
  const result = await Users.findOne({ username }).select('-__v');
  return result;
};
