const Schedule = require("./schema/schedule");

exports.addScheduleData = async (payload) => {
  const schedule = new Schedule(payload);
  const result = schedule.save();
  return result;
};

exports.getScheduleData = async (usersId) => {
  const result = await Schedule.find({ usersId }).select("-__v");
  return result;
};
