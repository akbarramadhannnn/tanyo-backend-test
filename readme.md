### - Installation
- clone repositories ```git clone https://gitlab.com/akbarramadhannnn/tanyo-backend-test.git```
- write on your cmd ```cd tanyo-backend-test```
- run ```npm install```
- run ```npm run start:dev```
- server running on ```http://localhost:6000```

### - Endpoint
| Description         | Method                         | Endpoint          |
| ------------------- | ------------------------------ | ----------------- |
| Add Users           | POST                           | /api/users        |
| Login               | POST                           | /api/auth/login   |
| Get/Add Schedule    | GET/POST                       | /api/schedule     |

### - Usage Example Api

#### 1. Add Users

URL
```
POST http://localhost:6000/api/users
```

Sample Request Body

```
{
    "fullName": "Johan",
    "username": "johan",
    "password": "123johan"
}
```
Sample Response:

```
{
    "success": true,
    "status": 201,
    "message": "Added Users Successfully",
    "result": {}
}
```

#### 2. Login

URL
```
POST http://localhost:6000/api/auth/login
```

Sample Request Body

```
{
    "username": "johan",
    "password": "123johan"
}
```
Sample Response:

```
{
    "success": true,
    "status": 200,
    "message": "Sign in Successfully",
    "result": {
        "token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2Mzg4YzBjNWY1ZDU1MDBlYTA0Njk2YzQiLCJpYXQiOjE2Njk5OTYxMTQsImV4cCI6MTY2OTk5OTcxNH0.bs6TEAmgCq5MMm2M2u_Lkwhukd2APgBt23n78y5el3kiQL8wHfz931je12r9Emooc641VLUL38njJa_0OfokVQ"
    }
}
```

#### 3. Add Schedule

URL
```
POST http://localhost:6000/api/schedule
```

Request Body ```Authorization: Bearer <token>```
```
{
    "from": "2022-02-25",
    "to": "2022-03-25",
    "time": "0",        // ["0", "1", "2"]
    "consultType": "0"  // ["0", "1", "2"]
}
```
Sample Response:

```
{
    "success": true,
    "status": 201,
    "message": "Added Schedule Successfully",
    "result": {}
}
```

#### 4. Get Schedule

URL
```
GET http://localhost:6000/api/schedule
```

Sample Response ```Authorization: Bearer <token>```

```
{
    "success": true,
    "status": 200,
    "message": "Get Schedule Successfully",
    "result": [
        {
            "scheduleId": "638a21b0bfbaf6da659e6b64",
            "from": "25 Februari 2022",
            "to": "25 Maret 2022",
            "time": {
                "value": "0",
                "startTime": "09.00 AM",
                "endTime": "09.15 AM",
                "price": "IDR 200.000",
                "duration": "15 min"
            },
            "consultType": "Chat"
        }
        ...
    ]
}
```
