const router = require("express").Router();
const { addSchedule, getSchedule } = require("../../controllers/schedule");
const { cekToken } = require("../../middleware/auth");

router.post("/", cekToken, addSchedule);
router.get("/", cekToken, getSchedule);

module.exports = router;
