const router = require("express").Router();
const auth = require("./auth");
const user = require("./user");
const schedule = require("./schedule");

router.use("/auth", auth);
router.use("/users", user);
router.use("/schedule", schedule);

module.exports = router;
