const router = require("express").Router();
const { addUsers } = require("../../controllers/user");

router.post("/", addUsers);

module.exports = router;
