const listDate = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember",
];

const convertDate = (date) => {
  const newDate = new Date(date);

  const result = `${newDate.getDate()} ${
    listDate[newDate.getMonth()]
  } ${newDate.getFullYear()}`;

  return result;
};

module.exports = convertDate;
