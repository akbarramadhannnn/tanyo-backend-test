const bcryptjs = require("bcryptjs");
const { addUserData, getUserByUsername } = require("../models/usersModels");
const Response = require("../helpers/response");

exports.addUsers = async (req, res) => {
  const { fullName, username, password } = req.body;

  if (!fullName || fullName === "") {
    return res.json(
      Response(false, 400, `Full Name is Required`, {
        name: "fullName",
      })
    );
  }

  if (!username || username === "") {
    return res.json(
      Response(false, 400, `Username is Required`, {
        name: "username",
      })
    );
  }

  if (!password || password === "") {
    return res.json(
      Response(false, 400, `Password is Required`, {
        name: "password",
      })
    );
  }

  const user = await getUserByUsername(username);

  if (user) {
    return res.json(
      Response(false, 400, `Username Already`, {
        name: "username",
      })
    );
  }

  const salt = await bcryptjs.genSalt(10);
  const hashPassword = await bcryptjs.hash(password, salt);

  const payload = {
    fullName,
    username,
    password: hashPassword,
  };

  await addUserData(payload);
  return res.json(Response(true, 201, "Added Users Successfully", {}));
};
