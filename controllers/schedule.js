const {
  addScheduleData,
  getScheduleData,
} = require("../models/scheduleModels");
const convertDate = require("../helpers/convertDate");
const timeList = require("../data/timeList");
const Response = require("../helpers/response");

exports.addSchedule = async (req, res) => {
  const { from, to, time, consultType } = req.body;
  const { userId } = req.userData;

  if (!from || from === "") {
    return res.json(
      Response(false, 400, `From is Required`, {
        name: "from",
      })
    );
  }

  if (!to || to === "") {
    return res.json(
      Response(false, 400, `To is Required`, {
        name: "to",
      })
    );
  }

  if (!time || time === "") {
    return res.json(
      Response(false, 400, `Time is Required`, {
        name: "time",
      })
    );
  }

  if (!consultType || consultType === "") {
    return res.json(
      Response(false, 400, `Consult Type is Required`, {
        name: "consultType",
      })
    );
  }

  const payload = {
    usersId: userId,
    from,
    to,
    time,
    consultType,
  };

  await addScheduleData(payload);
  return res.json(Response(true, 201, "Added Schedule Successfully", {}));
};

exports.getSchedule = async (req, res) => {
  const { userId } = req.userData;

  const result = await getScheduleData(userId);

  if (!result.length > 0) {
    return res.json(Response(true, 204, `Get Schedule Successfully`, {}));
  }

  const dataArr = [];
  for (let i = 0; i < result.length; i++) {
    const newObj = {};
    newObj.scheduleId = result[i]._id;
    newObj.from = convertDate(result[i].from);
    newObj.to = convertDate(result[i].to);
    newObj.time = {
      value: result[i].time,
      ...timeList[result[i].time],
    };
    newObj.consultType =
      result[i].consultType === "0"
        ? "Chat"
        : result[i].consultType === "1"
        ? "Video Call"
        : "Call";
    dataArr.push(newObj);
  }

  return res.json(Response(true, 200, `Get Schedule Successfully`, dataArr));
};
