const bcryptjs = require("bcryptjs");
const { getUserByUsername } = require("../models/usersModels");
const { signJWT } = require("../middleware/auth");
const Response = require("../helpers/response");

exports.login = async (req, res) => {
  const { username, password } = req.body;

  if (!username || username === "") {
    return res.json(
      Response(false, 400, `Username is Required`, {
        name: "username",
      })
    );
  }

  if (!password || password === "") {
    return res.json(
      Response(false, 400, `Password is Required`, {
        name: "password",
      })
    );
  }

  let isUsername = false;
  let userData = {};

  const user = await getUserByUsername(username);

  if (user) {
    isUsername = true;
    userData = user;
  }

  if (!isUsername) {
    return res.json(Response(false, 400, `Username / Password Salah`, {}));
  }

  const isUserPassword = await bcryptjs.compare(password, userData.password);

  if (!isUserPassword) {
    return res.json(Response(false, 400, `Username / Password Salah`, {}));
  }

  let payload = {};
  payload.userId = userData._id.toHexString();

  const token = signJWT(payload);

  return res.json(
    Response(true, 200, "Sign in Successfully", {
      token: token,
    })
  );
};
