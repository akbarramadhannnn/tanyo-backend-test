const mongoose = require("mongoose");
const configEnv = require("../config/env");

mongoose
  .connect(configEnv.mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log(`Database terhubung.. nama mongodb uri ${configEnv.mongoURI}`);
  })
  .catch((error) => {
    console.log(`Database koneksi error: ${error.message}`);
  });

module.exports = mongoose;
